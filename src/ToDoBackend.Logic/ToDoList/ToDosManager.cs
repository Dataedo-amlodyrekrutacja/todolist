﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ToDoBackend.DataAccess.Models;

namespace ToDoBackend.Logic.ToDoList
{
    public interface IToDosManager
    {
        Task<ToDos> Create(long? id, string content, DateTimeOffset deadline, string localization, CancellationToken token = default);
        Task<List<ToDos>> Read(long? listingId, CancellationToken token = default);
        Task<bool> Update(long? id, string content, string localization, DateTimeOffset deadline, CancellationToken token = default);
        Task<bool> Delete(long? id, CancellationToken token = default);
    }
    public class ToDosManager : IToDosManager
    {
        private readonly IToDosRepository repo;
        public ToDosManager(IToDosRepository repo)
        {
            this.repo = repo;
        }

        public async Task<ToDos> Create(long? id, string title, DateTimeOffset deadline, string description, CancellationToken token = default)
        {
            var toDos = new ToDos
            {
                Title = title,
                Description = description,
                ListingId = id,
                Deadline = deadline
            };
            await repo.CreateToDos(toDos, token);
            return toDos;
        }

        public async Task<bool> Delete(long? id, CancellationToken token = default)
        {

            if (id == null)
            {
                return false;
            }
            var toDos = repo.ToDoses
                .Where(l => l.Id == id)
                .FirstOrDefault();
            if (toDos == null)
            {
                return false;
            }
            await repo.DeleteToDos(toDos, token);
            return true;
        }

        public async Task<List<ToDos>> Read(long? listingId, CancellationToken token = default)
        {
            var result = await repo.ToDoses
                .Where(l => l.ListingId == listingId)
                .AsNoTracking()
                .ToListAsync(token);
            return result;
        }

        public async Task<bool> Update(long? id, string title, string description, DateTimeOffset deadline, CancellationToken token = default)
        {
            if (id == null)
            {
                return false;
            }
            var toDos = repo.ToDoses
               .Where(l => l.Id == id)
               .FirstOrDefault();
            if (toDos == null)
            {
                return false;
            }
            toDos.Title = title;
            toDos.Description = description;
            toDos.Deadline = deadline;
            await repo.SaveToDos(toDos, token);
            return true;
        }
    }
}
