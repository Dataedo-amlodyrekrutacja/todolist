﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ToDoBackend.DataAccess.Models;

namespace ToDoBackend.Logic.ToDoList
{
    public interface IListManager
    {
        Task<ToDoListing> Create(string userId, string title, CancellationToken token = default);
        Task<List<ToDoListing>> Read(string userId, CancellationToken token = default);
        Task<ToDoListing> ReadOne(long listingId, CancellationToken token = default);
        Task<bool> Update(long? id, string title, CancellationToken token = default);
        Task<bool> Delete(long? id, CancellationToken token = default);
    }
    public class ListManager : IListManager
    {
        private readonly IListingRepository repo;
        private ToDoListing listing = new ToDoListing();
        public ListManager(IListingRepository repo)
        {
            this.repo = repo;
        }

        public async Task<ToDoListing> Create(string userId, string title, CancellationToken token = default)
        {
            listing.Title = title;
            listing.UserId = userId;
            await repo.CreateListing(listing, token);
            return listing;
        }

        public async Task<bool> Delete(long? id, CancellationToken token = default)
        {
            if (id == null)
            {
                return false;
            }
            listing = repo.Listings
                .Where(l => l.Id  == id)
                .FirstOrDefault();
            if (listing == null)
            {
                return false;
            }
            await repo.DeleteListing(listing, token);
            return true;
        }

        public async Task<List<ToDoListing>> Read(string userId, CancellationToken token = default)
        {
            var result = await repo.Listings
                .Where(l => l.UserId == userId)
                .AsNoTracking()
                .ToListAsync();
            return result;
        }

        public async Task<ToDoListing> ReadOne(long listingId, CancellationToken token = default)
        {
            var result = await repo.Listings
                .Where(l => l.Id == listingId)
                .AsNoTracking()
                .FirstOrDefaultAsync();
            return result;
        }

        public async Task<bool> Update(long? id, string title, CancellationToken token = default)
        {
            if (id == null)
            {
                return false;
            }
            listing = repo.Listings
               .Where(l => l.Id == id)
               .FirstOrDefault();
            if (listing == null)
            {
                return false;
            }
            listing.Title = title;
            await repo .SaveListing(listing, token);
            return true;
        }
    }
}
