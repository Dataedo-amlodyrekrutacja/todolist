﻿using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using ToDoBackend.DataAccess.Models;

namespace ToDoBackend.Logic.UserManagement
{
    public interface IUserLoginManager
    {
        Task<bool> TrySignIn(string username, string password);
        Task<bool> TrySignOut();
    }

    public class UserLoginManager : IUserLoginManager
    {
        private UserManager<User> userManager;
        private SignInManager<User> signInManager;

        public UserLoginManager(UserManager<User> userManager, SignInManager<User> signInManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        public async Task<bool> TrySignIn(string username, string password)
        {
            var user = await userManager.FindByNameAsync(username);
            if (user == null)
            {
                return false;
            }
            await signInManager.SignOutAsync();
            var result = await signInManager.PasswordSignInAsync(user, password, false, false);
            if (result != SignInResult.Success)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> TrySignOut()
        {
            await signInManager.SignOutAsync();
            return true;
        }
    }
}
