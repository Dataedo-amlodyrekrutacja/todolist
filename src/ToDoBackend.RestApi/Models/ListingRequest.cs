﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace ToDoBackend.RestApi.Models
{
    public class ListingRequest
    {
        [BindProperty]
        [Required]
        [StringLength(40, MinimumLength = 3)]
        public string Title { get; set; }
    }
}
