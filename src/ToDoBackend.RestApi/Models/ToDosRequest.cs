﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;

namespace ToDoBackend.RestApi.Models
{
    public class ToDosRequest
    {
        [BindProperty]
        [Required]
        [StringLength(40, MinimumLength = 3)]
        public string Title { get; set; }

        [BindProperty]
        [Required]
        [StringLength(100, MinimumLength = 3)]
        public string Description { get; set; }
        [BindProperty]
        public DateTimeOffset Deadline { get; set; }
    }
}
