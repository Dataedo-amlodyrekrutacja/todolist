﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;
using ToDoBackend.Logic.ToDoList;
using ToDoBackend.RestApi.Models;
using System.Linq;
using System.Collections.Generic;
using System.Threading;

namespace ToDoBackend.RestApi.Controllers
{
    [Authorize]
    [Route("Listing")]
    public class ListingController : Controller
    {
        private IListManager listManager;
        private IUserChecker checker;
        public ListingController(IListManager listManager, IUserChecker checker)
        {
            this.listManager = listManager;
            this.checker = checker;
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] ListingRequest listingRequest, CancellationToken token = default)
        {
            var userId = HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var listing = await listManager.Create(userId, listingRequest.Title, token);
            return Ok(listing.Id);
        }

        [Route("{id}")]
        [HttpPut]
        public async Task<ActionResult> Update([FromBody] ListingRequest listingRequest, [FromRoute] long? id, CancellationToken token = default)
        {
            var userId = HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (userId == null)
            {
                return BadRequest();
            }

            var checkResult = await checker.CheckUser(id, userId);

            if (!checkResult)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = await listManager.Update(id, listingRequest.Title, token);
            if (!result)
            {
                return BadRequest(result);
            }
            return NoContent();
        }

        [Route("{id}")]
        [HttpDelete]
        public async Task<ActionResult> Delete([FromRoute] long? id, CancellationToken token = default)
        {
            var userId = HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (userId == null)
            {
                return BadRequest();
            }

            var checkResult = await checker.CheckUser(id, userId);

            if (!checkResult)
            {
                return NotFound();
            }

            if (id == null)
            {
                return BadRequest();
            }
            await listManager.Delete(id, token);
            return Ok();
        }

        [HttpGet]
        public async Task<List<ListingResponse>> ReadAllListings(CancellationToken token = default)
        {
            var userId = HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            var listing = await listManager.Read(userId, token);

            var response = listing.Select(a => new ListingResponse() {Id = a.Id, Title = a.Title}).ToList();

            return response;
        }
    }
}
