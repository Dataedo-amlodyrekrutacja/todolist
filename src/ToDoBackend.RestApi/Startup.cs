using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ToDoBackend.DataAccess.Models;
using ToDoBackend.Logic.UserManagement;
using ToDoBackend.Logic.ToDoList;

namespace ToDoBackend.RestApi
{
    public class Startup
    {
        public Startup(IConfiguration config)
        {
            Configuration = config;
        }
        public IConfiguration Configuration { get; set; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<IdentityContext>(opts =>
                opts.UseSqlServer(Configuration ["ConnectionStrings:IdentityConnection"]));
            services.AddIdentity<User, IdentityRole>()
                .AddEntityFrameworkStores<IdentityContext>();

            services.AddAuthentication();
            services.AddAuthorization();
            services.AddControllers();

            services.AddTransient<IUserLoginManager, UserLoginManager>();
            services.AddTransient<IUserCreatorManager, UserCreatorManager>();
            services.AddTransient<IUserAccManager, UserAccManager>();
            services.AddTransient<IListManager, ListManager>();
            services.AddTransient<IToDosManager, ToDosManager>();
            services.AddTransient<IUserChecker, UserChecker>();
            services.AddScoped<IListingRepository, EFListingRepository>();
            services.AddScoped<IToDosRepository, EFToDosRepository>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
