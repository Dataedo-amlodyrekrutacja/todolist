﻿using System.Collections.Generic;

namespace ToDoBackend.DataAccess.Models
{
    public class ToDoListing
    {
        public long Id { get; set; }
        public string UserId { get; set; }
        public User Owner { get; set; }
        public string Title { get; set; }
        public virtual ICollection<ToDos> ToDoses { get; set; } = new HashSet<ToDos>();
    }
}
