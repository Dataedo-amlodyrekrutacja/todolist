﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ToDoBackend.DataAccess.Models
{
    public class IdentityContext : IdentityDbContext<User>
    {
        public DbSet<ToDoListing> Listings { get; set; }
        public DbSet<ToDos> ToDoses { get; set; }

        public IdentityContext(DbContextOptions<IdentityContext> options)
            : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            var user = builder.Entity<User>();
            user.Property(x => x.Name).IsRequired();
            user.Property(x => x.Surname).IsRequired();
            user.HasMany(x => x.Listings)
                .WithOne(x => x.Owner)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            var listing = builder.Entity<ToDoListing>();
            listing.Property(x => x.Title).IsRequired();
            listing.HasKey(x => x.Id);
            listing.HasMany(x => x.ToDoses)
                .WithOne(x => x.Listing)
                .HasForeignKey(x => x.ListingId)
                .OnDelete(DeleteBehavior.Cascade);

            var todos = builder.Entity<ToDos>();
            todos.Property(x => x.Title).IsRequired();
            todos.Property(x => x.Description).IsRequired();
            todos.HasKey(x => x.Id);
        }
    }
}