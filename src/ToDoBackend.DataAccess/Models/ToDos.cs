﻿using System;

namespace ToDoBackend.DataAccess.Models
{
    public class ToDos
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public long? ListingId { get; set; }
        public ToDoListing Listing { get; set; }
        public string Description { get; set; }
        public DateTimeOffset? Deadline { get; set; }
    }
}
