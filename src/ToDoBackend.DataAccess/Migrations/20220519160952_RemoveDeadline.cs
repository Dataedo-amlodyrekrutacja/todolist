﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ToDoBackend.DataAccess.Migrations
{
    public partial class RemoveDeadline : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Deadline",
                table: "Listings");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Deadline",
                table: "Listings");
        }
    }
}
