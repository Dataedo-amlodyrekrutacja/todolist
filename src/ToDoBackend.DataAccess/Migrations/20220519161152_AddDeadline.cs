﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace ToDoBackend.DataAccess.Migrations
{
    public partial class AddDeadline : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "Deadline",
                table: "ToDoses",
                type: "DateTimeOffset",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "Deadline",
                table: "ToDoses",
                type: "DateTimeOffset",
                nullable: true);
        }
    }
}
