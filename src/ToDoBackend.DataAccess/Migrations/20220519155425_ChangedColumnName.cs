﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ToDoBackend.DataAccess.Migrations
{
    public partial class ChangedColumnName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Deadline",
                table: "Listings");

            migrationBuilder.RenameColumn(
                name: "Localization",
                table: "ToDoses",
                newName: "Title");

            migrationBuilder.RenameColumn(
                name: "Content",
                table: "ToDoses",
                newName: "Description");

            migrationBuilder.DropColumn(
                name: "Deadline",
                table: "ToDoses");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Deadline",
                table: "ToDoses");

            migrationBuilder.RenameColumn(
                name: "Title",
                table: "ToDoses",
                newName: "Localization");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "ToDoses",
                newName: "Content");

            migrationBuilder.DropColumn(
                name: "Deadline",
                table: "Listings");
        }
    }
}
